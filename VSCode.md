tasks.json
-----

```javascript
{
  // See https://go.microsoft.com/fwlink/?LinkId=733558
  // for the documentation about the tasks.json format
  "version": "2.0.0",
  "tasks": [
    {
      "label": "Django server",
      "type": "shell",
      "command": ". ~/.bash_profile; workon dashboard-backend; python manage.py runserver 0.0.0.0:8091",
      "problemMatcher": []
    }
  ]
}
```

settings.json
--------

```javascript
{
  "python.pythonPath": "C:\\Users\\Vittorio\\envs\\dashboard-backend\\Scripts\\python.exe"
}
```