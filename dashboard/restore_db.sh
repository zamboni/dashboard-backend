showHelp=0
restoreFinance=0
loadContextsFixtures=0
loadMoneyMovementsFixtures=0
settings="dashboard.settings.dashboard"

while getopts 'fmcph' flag; do
	case "${flag}" in
	  h) showHelp=1 ;;
		f) restoreFinance=1 ;;
		c) loadContextsFixtures=1 ;;
		m) loadMoneyMovementsFixtures=1 ;;
		p) settings="dashboard.settings.production" ;;
		*) error "Unexpected option ${flag}" ;;
	esac
done

echo "Using settings $settings";

if (( $showHelp > 0 )); then
	echo "Help commands:";
	echo -e "\t-h show this help and exit;";
	echo -e "\t-f remove old finance migrations and generate from scratch;";
	echo -e "\t-c load contexts samples;";
	echo -e "\t-m load money movements samples;";
	echo -e "\t-p use production settings (dashboard.settings.production);";
	exit 0
fi

echo "Will do:";
if [ "$settings" == "dashboard.settings.dashboard" ]; then echo -e "\t- remove the sqlite db (dashboard/db.sqlite3)"; fi
if (( $restoreFinance > 0 )); then echo -e "\t- Remove finance migrations"; fi
echo -e "\t- Migrate"
echo -e "\t- Reload categories";
if (( $loadContextsFixtures > 0 )); then echo -e "\t- Load Contexts samples"; fi
if (( $loadMoneyMovementsFixtures > 0 )); then echo -e "\t- Load Money Movements samples"; fi
read -p "Press Enter to continue or CTRL+C to exit"

if [ "$settings" == "dashboard.settings.dashboard" ]; then
	rm dashboard/db.sqlite3
fi

if (( $restoreFinance > 0 )); then
	echo "Remove old finance migrations"
	rm ~/repositories/finance/finance/migrations/0*.py*
	python manage.py makemigrations finance --settings="$settings"
fi

echo "Migrate"
python manage.py migrate --settings="$settings"

echo "Reload categories"
python manage.py finance_load_categories ~/repositories/finance/finance/fixtures/categories.json --settings="$settings"

if (( $loadContextsFixtures > 0 )); then
	echo "Load Contexts samples"
	python manage.py finance_load_contexts ~/repositories/finance/finance/fixtures/contexts.json --settings="$settings"
fi
if (( $loadMoneyMovementsFixtures > 0 )); then
	echo "Load Money Movements samples"
	python manage.py finance_load_money_movements ~/repositories/finance/finance/fixtures/money_movements.json --settings="$settings"
fi
