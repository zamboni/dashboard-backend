#from django.contrib.auth.models import User
from django.db import models
from django.conf import settings

"""
Common models, referenced by other models as foreign keys
"""


class ObjectStatus(models.Model):
    """Object statuses, grouped by model class name.
    I.e. 'created' and 'sequenced' for Library model, connect with a
    ForeignKey with limit_choices_to={'model_class_name_tag': 'Library'} .

    :Parameters:
        - `label`: descriptive name of the status (i.e. Created)
        - `codename`: slugged unique identifier of the status (i.e. created)
        - `model_class_name_tag`: Model class name, used to group statuses,
            with an optional tag (if a model has more than one status,
            i.e. lims.Library.sequencing_status and lims.Library.preparation_status)
        - `order`: optional order, used for ordering
        - `description`: optional description of the status
    """
    label = models.CharField(max_length=255)
    codename = models.CharField(max_length=255, blank=True)
    model_class_name_tag = models.CharField(max_length=255)
    order = models.SmallIntegerField(default=0)

    class Meta:
        unique_together = (('codename', 'model_class_name_tag'), )
        ordering = ('model_class_name_tag', 'order', 'label')
        verbose_name_plural = 'object statuses'

    def __unicode__(self):
        return '%s' % self.label

    def save(self, *args, **kwargs):
        super(ObjectStatus, self).save(*args, **kwargs)


"""
Audit models
"""


class AuditLogMixin(models.Model):
    """
    Add these fields to your model:

    from AuditLogMixin:
    :parameter: creation_date
    :parameter: created_by
    :parameter: last_edit_date
    :parameter: last_edit_by
    :parameter: audit_user (used to automatically set created_by and last_edit_by)
    """

    creation_date = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(getattr(settings, 'AUTH_USER_MODEL', 'auth.User'),
        related_name="created_%(app_label)s_%(class)s_set",
        null=True, blank=True)
    last_edit_date = models.DateTimeField(auto_now=True)
    last_edit_by = models.ForeignKey(getattr(settings, 'AUTH_USER_MODEL', 'auth.User'),
        related_name="edited_%(app_label)s_%(class)s_set",
        null=True, blank=True)

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super(AuditLogMixin, self).__init__(*args, **kwargs)
        self.audit_user = None

    def save(self, *args, **kwargs):
        audit_user = kwargs.pop('audit_user', None)
        if audit_user:
            self.audit_user = audit_user
        if self.audit_user:
            if not self.creation_date:
                self.created_by = self.audit_user
            self.last_edit_by = self.audit_user
        super(AuditLogMixin, self).save(*args, **kwargs)

    def auditlog_register_creation(self, user):
        self.created_by = user
        self.last_edit_by = user

    def auditlog_register_edit(self, user):
        self.last_edit_by = user
