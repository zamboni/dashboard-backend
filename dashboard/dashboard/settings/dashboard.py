from corsheaders.defaults import default_headers

from .common import *  # pylint: disable=unused-import

BASE_URL = 'http://localhost:8091'

INSTALLED_APPS += [
    'corsheaders',
    'django_extensions',
    'djmoney',
    'tagulous',
    'rest_framework',
    'rest_framework.authtoken',
    'taggit_serializer',

    'dashboard',
    'finance',
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    #'DEFAULT_PERMISSION_CLASSES': [
    #    'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    #],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 20,
}

CORS_ORIGIN_WHITELIST = (
    'http://localhost:8090',  # frontend port - VUE
    'http://localhost:3000',  # frontend port - React
    'http://localhost:5000',  # frontend port - React Build
)

CORS_ALLOW_HEADERS = default_headers + (
    # 'Token'
)

# Tagolous
SERIALIZATION_MODULES = {
    'json':   'tagulous.serializers.json',
    'python': 'tagulous.serializers.python',
}

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]


ALLOWED_HOSTS = ['192.168.0.108', 'localhost']

INTERNAL_IPS = [
    'http://localhost:8091',
    'http://192.168.0.108:8091',
    '127.0.0.1',
]

CORS_ORIGIN_WHITELIST = (
    'http://localhost:8090',  # frontend port - VUE
    'http://localhost:3000',  # frontend port - React
    'http://192.168.0.108:3000',  # frontend port - React
    'http://localhost:5000',  # frontend port - React Build
)

try:
    from local_settings import *
except ImportError:
    pass

MIDDLEWARE += [
    'axes.middleware.AxesMiddleware',
]
