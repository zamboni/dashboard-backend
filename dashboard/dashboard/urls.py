from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin

from rest_framework.authtoken import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/', views.obtain_auth_token, name='api_token_auth'),
    url(r'^dashboard/api/', include('dashboard.api.urls', namespace='dashboard')),
    url(r'^finance/', include('finance.urls', namespace='finance')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns
