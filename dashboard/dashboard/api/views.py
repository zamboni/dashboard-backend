from django.contrib.auth.models import User
from django.http import JsonResponse

from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from . import serializers


"""
Views
"""

class DashboardView(APIView):

    def get(self, request, format_r=None):
        return Response({
            'logged-user': reverse('dashboard:logged_user', request=request, format=format_r),
        })


class LoggedUserView(RetrieveAPIView):
    serializer_class = serializers.UserSerializer

    def retrieve(self, request, *args, **kwargs):
        serializer = self.get_serializer(request.user)
        return Response(serializer.data)
