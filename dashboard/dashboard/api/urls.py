from django.conf.urls import url, include

from rest_framework import routers

from . import views

router = routers.DefaultRouter()

urlpatterns = [
    url(r'^$', views.DashboardView.as_view(), name='dashboard'),

    url(r'^logged-user/', views.LoggedUserView.as_view(), name='logged_user'),
]
