from django.contrib.auth.models import User

from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'full_name', 'is_superuser')

    def get_full_name(self, user):
        if hasattr(user, 'get_full_name'):
            return user.get_full_name()
        return ''

