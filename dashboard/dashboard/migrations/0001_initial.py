from django.contrib.auth.models import User
from django.db import migrations


def create_superuser(apps, schema_editor):
    superuser = User()
    superuser.is_active = True
    superuser.is_superuser = True
    superuser.is_staff = True
    superuser.username = 'admin'
    superuser.first_name = 'Admin'
    superuser.email = 'admin@admin.net'
    superuser.set_password('admin2018')
    superuser.save()

class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.RunPython(create_superuser)
    ]
