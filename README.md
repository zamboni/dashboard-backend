# dashboard

Base dashboard backend.

Installation
------------

```bash
# create a virtualenv
mkvirtualenv dashboard-backend

# clone the repository
git clone git@bitbucket.org:zamboni/dashboard-backend.git

# enter the folder and install requirements
cd dashboard-backend
pip install -r requirements.txt

# install developer dependencies
pip install -r requirements_dev.txt

# Add dashboard to python path in virtualenv
# *nix: add2virtualenv <path of this file>/dashboard
# windows: add to <virtualenv/lib/site-packages/_virtualenv_path_extensions.pth> <windows path to this file>\dashboard
#   if the file is not there, add2virtualenv c: and replace it

# Add other local applications to python path in virtualenv
# - finance

# enter the project folder and create the database
cd dashboard
python manage.py migrate

```

Run the server
--------------

```bash
python manage.py runserver 0.0.0.0:8091
```

Login to http://localhost:8091/admin/ with user `admin` - password `admin2018` and change the password.
